/**
 *
 * Asynchronously loads the component for ListRecommendations
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
