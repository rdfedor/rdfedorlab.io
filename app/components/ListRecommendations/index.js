/**
 *
 * ListRecommendations
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

// import { FormattedMessage } from 'react-intl';
import { Media, Image } from 'react-bootstrap';
import md5 from 'md5';
// import messages from './messages';

function ListRecommendations({ recommendations }) {
  return (
    <ul>
      {(recommendations || []).map(recommendation => (
        <Media key={md5(JSON.stringify(recommendation))}>
          <Image
            src={recommendation.profilePic}
            width={64}
            height={64}
            className="mr-3"
            alt={`${recommendation.name}`}
            roundedCircle
          />
          <Media.Body className="mr-4">
            <div className="font-weight-bold">
              {`${recommendation.name}, ${recommendation.title}`}
            </div>
            <div className="font-italic text-muted">
              {recommendation.byLine}
            </div>
            <p>{recommendation.text}</p>
          </Media.Body>
        </Media>
      ))}
    </ul>
  );
}

ListRecommendations.propTypes = {
  recommendations: PropTypes.arrayOf(PropTypes.object),
};

export default ListRecommendations;
