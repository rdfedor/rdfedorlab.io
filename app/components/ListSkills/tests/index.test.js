/**
 *
 * Tests for ListSkills
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider, defineMessages } from 'react-intl';
// import 'jest-dom/extend-expect'; // add some helpful assertions

import ListSkills from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<ListSkills />', () => {
  const scope = 'testscope';
  const messages = defineMessages({
    react: {
      id: `${scope}.react`,
      defaultMessage: 'React',
    },
    javascript: {
      id: `${scope}.javascript`,
      defaultMessage: 'Javascript',
    },
  });

  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListSkills skills={[]} />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Expect to not log errors in console without skills', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListSkills />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Expect to not log errors in console without skills', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListSkills
          {...{
            skills: [
              {
                name: messages.react,
                value: 40,
              },
              {
                name: messages.javascript,
                test: /(javascript)/i,
                value: 75,
              },
            ],
            positions: [
              {
                company: 'Test Company',
                duration: 'Test Duration',
                location: 'Austin, TX',
                position: 'Software Engineer',
                highlights: ['javascript', 'react'],
              },
            ],
          }}
        />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  /**
   * Unskip this test to use it
   *
   * @see {@link https://jestjs.io/docs/en/api#testskipname-fn}
   */
  it('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListSkills skills={[]} />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
