/**
 *
 * ListSkills
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import md5 from 'md5';
import { Row, Col, ProgressBar, Accordion, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';

// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import ListPositions from '../ListPositions';
// import messages from './messages';

function ListSkills({ skills, positions }) {
  return (
    <div className="p-3">
      {(skills || []).map(({ name, value, test }) => (
        <Accordion key={`${name.id}-${value}`}>
          <Row>
            <Col xs="1" className="m-1 text-center">
              <Accordion.Toggle
                as={Button}
                variant="outline-info"
                size="sm"
                eventKey={md5(`${name.id}-${value}`)}
                className={
                  !positions.filter(
                    p =>
                      p.highlights.filter(
                        h =>
                          (test && test.test && test.test(h)) ||
                          h.indexOf(test || name.defaultMessage) > -1,
                      ).length > 0,
                  ).length
                    ? 'd-none'
                    : ''
                }
              >
                <FontAwesomeIcon icon={faPlus} />
              </Accordion.Toggle>
            </Col>
            <Col className="m-2">
              <FormattedMessage {...name} />
            </Col>
            <Col className="m-2">
              <ProgressBar now={value} />
            </Col>
          </Row>
          <Accordion.Collapse eventKey={md5(`${name.id}-${value}`)}>
            <ListPositions
              positions={positions.filter(
                p =>
                  p.highlights.filter(
                    h =>
                      (test && test.test && test.test(h)) ||
                      h.indexOf(test || name.defaultMessage) > -1,
                  ).length > 0,
              )}
              highlightSkill={test && test.test ? test : name.defaultMessage}
            />
          </Accordion.Collapse>
        </Accordion>
      ))}
    </div>
  );
}

ListSkills.propTypes = {
  skills: PropTypes.arrayOf(PropTypes.object),
  positions: PropTypes.arrayOf(PropTypes.object),
};

export default ListSkills;
