/**
 *
 * ListDetails
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Row, Col } from 'react-bootstrap';
import md5 from 'md5';

import { FormattedMessage } from 'react-intl';

const StyledCol = styled(Col)`
  font-weight: bold;
  text-transform: uppercase;
`;

function ListDetails({ details, groupBy, location }) {
  const selectedGroupBy = Number.parseInt(groupBy, 10) || 1;
  const groupedDetails = [];
  const filteredDetails = details || [];
  let i;

  for (i = 0; i <= filteredDetails.length; i += selectedGroupBy) {
    groupedDetails.push(filteredDetails.slice(i, i + selectedGroupBy));
  }

  let columnSize = 3;

  if (selectedGroupBy === 1) {
    columnSize = 6;
  }

  return (
    <div key={`${location}-listdetails`} className="p-3">
      {groupedDetails.map(r => (
        <Row
          className="p-1"
          key={md5(`${location}-${JSON.stringify(r)}-${selectedGroupBy}`)}
        >
          {r.map(({ label, value }) => [
            <StyledCol
              key={md5(
                `${location}-${label.id}-${
                  typeof value === 'string' ? value : md5(JSON.stringify(value))
                }-label`,
              )}
              sm={columnSize}
            >
              <FormattedMessage {...label} />
            </StyledCol>,
            <Col
              key={md5(
                `${location}-${label.id}-${
                  typeof value === 'string' ? value : md5(JSON.stringify(value))
                }-value`,
              )}
              sm={columnSize}
            >
              {typeof value === 'string' ? (
                value
              ) : (
                <FormattedMessage {...value} />
              )}
            </Col>,
          ])}
        </Row>
      ))}
    </div>
  );
}

ListDetails.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object),
  location: PropTypes.string.isRequired,
  groupBy: PropTypes.number,
};

export default ListDetails;
