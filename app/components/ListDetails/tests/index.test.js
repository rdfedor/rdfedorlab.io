/**
 *
 * Tests for AboutMe
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider, defineMessages } from 'react-intl';
// import 'jest-dom/extend-expect'; // add some helpful assertions

import ListDetails from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<AboutMe />', () => {
  const scope = 'testscope';
  const messages = defineMessages({
    name: {
      id: `${scope}.name`,
      defaultMessage: 'Name',
    },
    age: {
      id: `${scope}.name`,
      defaultMessage: 'Age',
    },
    location: {
      id: `${scope}.location`,
      defaultMessage: 'Location',
    },
  });

  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListDetails details={[]} location="jest" />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Expect to not log errors in console without detais', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListDetails location="jest" />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Expect to not log errors in console when looping through details', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListDetails
          details={[
            {
              label: messages.name,
              value: 'Test Name',
            },
          ]}
          location="jest"
        />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Expect to not log errors in console when looping through details grouped by 2', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListDetails
          details={[
            {
              label: messages.name,
              value: 'Test Name',
            },
            {
              label: messages.age,
              value: '24 years',
            },
            {
              label: messages.location,
              value: 'Candy Land',
            },
          ]}
          location="jest"
          groupBy={2}
        />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  /**
   * Unskip this test to use it
   *
   * @see {@link https://jestjs.io/docs/en/api#testskipname-fn}
   */
  it('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListDetails details={[]} location="jest" />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
