/**
 *
 * StyledNav
 *
 */

// import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Nav } from 'react-bootstrap';

export default styled(Nav)`
  width: 100%;
`;
