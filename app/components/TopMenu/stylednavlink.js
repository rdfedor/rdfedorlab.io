/* eslint-disable no-irregular-whitespace */
/**
 *
 * StyledNavLink
 *
 */

// import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

export default styled(NavLink)`
  color: #000;
  padding: 1em !important;
  text-align: right;

  @media (min-width:900px) {
    width: 20%;
    text-align: center;
  }​
`;
