/**
 *
 * Tests for TopMenu
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { MemoryRouter } from 'react-router-dom';
// import 'jest-dom/extend-expect'; // add some helpful assertions

import TopMenu from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<TopMenu />', () => {
  it('Expect to render TopMenu not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <MemoryRouter>
          <TopMenu />
        </MemoryRouter>
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Expect contact me to open in new window', () => {
    window.open = jest.fn();
    const spy = jest.spyOn(window, 'open');

    const instance = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <MemoryRouter>
          <TopMenu />
        </MemoryRouter>
      </IntlProvider>,
    );

    fireEvent.click(instance.getByText(/contact me/i));

    expect(spy).toHaveBeenCalled();
  });

  /**
   * Unskip this test to use it
   *
   * @see {@link https://jestjs.io/docs/en/api#testskipname-fn}
   */
  it('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <MemoryRouter>
          <TopMenu />
        </MemoryRouter>
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
