/* eslint-disable no-irregular-whitespace */
/**
 *
 * StyledLink
 *
 */

// import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

export default styled.a`
  color: #000;
  padding: 1em !important;
  text-align: right;

  @media (min-width:900px) {
    width: 20%;
    text-align: center;
  }​
`;
