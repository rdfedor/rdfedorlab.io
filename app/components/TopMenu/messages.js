/*
 * TopMenu Messages
 *
 * This contains all the text for the TopMenu component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.TopMenu';

export default defineMessages({
  about_me: {
    id: `${scope}.about_me`,
    defaultMessage: 'About Me',
  },
  resume: {
    id: `${scope}.resume`,
    defaultMessage: 'Resume',
  },
  portfolio: {
    id: `${scope}.portfolio`,
    defaultMessage: 'Portfolio',
  },
  blog: {
    id: `${scope}.blog`,
    defaultMessage: 'Blog',
  },
  contact_me: {
    id: `${scope}.contact_me`,
    defaultMessage: 'Contact Me',
  },
  recommendations: {
    id: `${scope}.recommendations`,
    defaultMessage: 'Recomm',
  },
});
