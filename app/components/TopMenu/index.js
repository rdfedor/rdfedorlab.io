/**
 *
 * TopMenu
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { Navbar, Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faUser,
  faBookOpen,
  faProjectDiagram,
  faPencilAlt,
  faComments,
} from '@fortawesome/free-solid-svg-icons';
import { FormattedMessage } from 'react-intl';

import StyledLink from './styledlink';
import StyledNav from './stylednav';
import StyledNavLink from './stylednavlink';
import StyledNavbar from './stylednavbar';
import StyledNavbarBrand from './stylednavbarbrand';
import { gtag } from '../../utils/gtag';

import messages from './messages';

function TopMenu() {
  return (
    <StyledNavbar collapseOnSelect expand="lg" fixed="top">
      <Container>
        <StyledNavbarBrand href="#home" className="ml-2">
          Roger Fedor
        </StyledNavbarBrand>
        <Navbar.Toggle aria-controls="top-menu" className="ml-auto m-2" />
        <Navbar.Collapse id="top-menu">
          <StyledNav className="text-uppercase">
            <StyledNavLink className="nav-link" exact to="/">
              <FontAwesomeIcon className="mr-2" icon={faUser} />
              <FormattedMessage {...messages.about_me} />
            </StyledNavLink>
            <StyledNavLink className="nav-link" to="/resume">
              <FontAwesomeIcon className="mr-2" icon={faBookOpen} />
              <FormattedMessage {...messages.resume} />
            </StyledNavLink>
            <StyledNavLink className="nav-link" to="/recommendations">
              <FontAwesomeIcon className="mr-2" icon={faComments} />
              <FormattedMessage {...messages.recommendations} />
            </StyledNavLink>
            <StyledNavLink className="nav-link" to="/portfolio">
              <FontAwesomeIcon className="mr-2" icon={faProjectDiagram} />
              <FormattedMessage {...messages.portfolio} />
            </StyledNavLink>
            <StyledLink
              className="nav-link"
              href="#"
              onClick={e => {
                const email = '4864686-rdfedor@users.noreply.gitlab.com';
                e.stopPropagation();
                // Push notification to Google Analytics
                gtag('event', 'contact', {
                  event_category: 'Email Enquiry',
                  event_action: 'Mailto Click',
                  event_label: email,
                });
                window.open(`mailto:${email}`, '_blank');
                return false;
              }}
              active={false}
            >
              <FontAwesomeIcon className="mr-2" icon={faPencilAlt} />
              <FormattedMessage {...messages.contact_me} />
            </StyledLink>
          </StyledNav>
        </Navbar.Collapse>
      </Container>
    </StyledNavbar>
  );
}

TopMenu.propTypes = {};

export default TopMenu;
