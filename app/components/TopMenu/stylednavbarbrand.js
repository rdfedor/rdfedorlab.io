/* eslint-disable no-irregular-whitespace */
/**
 *
 * StyledNavLink
 *
 */

// import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Navbar } from 'react-bootstrap';

export default styled(Navbar.Brand)`
  @media (min-width:900px) {
    display: none;
  }​
`;
