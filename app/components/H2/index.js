/**
 *
 * H2
 *
 */

// import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const styledH2 = styled.h2`
  font-weight: bold;
  font-size: 1.25em;
  padding: 0.5em 0.75em;
  text-transform: uppercase;
`;

export default styledH2;
