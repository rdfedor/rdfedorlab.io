/**
 *
 * PaneDiv
 *
 */

// import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledDivPane = styled.div`
  background-color: #fff;
  padding-bottom: 1px;
  margin-bottom: 30px;
`;

export default StyledDivPane;
