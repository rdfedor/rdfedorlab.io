/**
 *
 * H2
 *
 */

// import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const styledH3 = styled.h3`
  font-weight: bold;
  font-size: 1.15em;
  padding: 0.5em 0;
`;

export default styledH3;
