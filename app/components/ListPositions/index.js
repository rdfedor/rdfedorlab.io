/**
 *
 * ListPositions
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Image } from 'react-bootstrap';
import md5 from 'md5';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

const StyledImage = styled(Image)`
  max-width: 139px;
  max-height: 70px;
  position: absolute;
  top: -10px;
  right: 0;
`;

const PositionHeaderDiv = styled.div`
  padding-right: 145px;
`;

function ListPositions({ positions, highlightSkill }) {
  return (
    <>
      {(positions || []).map(position => (
        <div key={md5(JSON.stringify(position))} className="ml-4 mr-4 pt-2">
          <PositionHeaderDiv className="pb-2 position-relative">
            <StyledImage
              src={position.image_src}
              className={`float-right${!position.image_src ? ' d-none' : ''}`}
              fluid
            />
            <span className="font-weight-bold font-italic">
              {`${position.company} - ${position.location}`}
            </span>
            <br />
            <span>{`${position.position}, ${position.duration}`}</span>
          </PositionHeaderDiv>
          <ul className="pt-2">
            {position.highlights
              .filter(
                highlight =>
                  !highlightSkill ||
                  (highlightSkill.test && highlightSkill.test(highlight)) ||
                  highlight.indexOf(highlightSkill) > -1,
              )
              .map(highlight => (
                <li key={md5(highlight)}>{highlight}</li>
              ))}
          </ul>
        </div>
      ))}
    </>
  );
}

ListPositions.propTypes = {
  positions: PropTypes.arrayOf(PropTypes.object),
  highlightSkill: PropTypes.any,
};

export default ListPositions;
