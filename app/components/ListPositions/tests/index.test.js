/**
 *
 * Tests for ListExpriences
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
// import 'jest-dom/extend-expect'; // add some helpful assertions

import ListExpriences from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<ListExpriences />', () => {
  const samplePositions = {
    company: 'Test Company',
    duration: 'June 2014 - present',
    location: 'Austin, TX',
    position: 'Software Engineer',
    highlights: ['did php stuff', 'did javascript stuff'],
  };

  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListExpriences />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Show php but not javascript', () => {
    const spy = jest.spyOn(global.console, 'error');

    const instance = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListExpriences positions={[samplePositions]} highlightSkill={/php/i} />
      </IntlProvider>,
    );

    expect(spy).not.toHaveBeenCalled();
    expect(instance.getByText(/php/i)).not.toBe(null);
  });

  /**
   * Unskip this test to use it
   *
   * @see {@link https://jestjs.io/docs/en/api#testskipname-fn}
   */
  it('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <ListExpriences />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
