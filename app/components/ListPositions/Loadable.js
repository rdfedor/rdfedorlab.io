/**
 *
 * Asynchronously loads the component for ListExpriences
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
