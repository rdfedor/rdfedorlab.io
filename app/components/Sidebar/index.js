/**
 *
 * Sidebar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Image, Row, Col, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedin, faGitlab } from '@fortawesome/free-brands-svg-icons';
import { FormattedMessage } from 'react-intl';
import { faFilePdf } from '@fortawesome/free-solid-svg-icons';
import LocaleToggle from '../../containers/LocaleToggle';
import ListDetails from '../ListDetails';
import DivPanel from '../DivPanel';
import H1 from '../H1';
import H2 from '../H2';
import messages from './messages';
import Photo from '../../images/photo.jpg';
import Resume from '../../documents/20220418-Fedor_Roger-Resume.pdf';
import { gtag } from '../../utils/gtag';

function Sidebar({ details }) {
  return (
    <DivPanel>
      <H2>
        <FormattedMessage {...messages.header} />
      </H2>
      <Image src={Photo} alt="Profile Photo" fluid />
      <ListDetails location="sidebar" details={details} />
      <hr />
      <H2>
        <FormattedMessage {...messages.attachments} />
      </H2>
      <hr />
      <div className="p-4">
        <Button
          variant="outline-secondary"
          href={Resume}
          target="_blank"
          onClick={() => {
            // Push notification to Google Analytics
            gtag('event', 'download', {
              event_category: 'download',
              event_action: 'pdf',
              event_label: 'Resume',
            });
          }}
          block
        >
          <Row>
            <Col xs="2" className="text-right">
              <FontAwesomeIcon
                icon={faFilePdf}
                size="lg"
                className="pull-left"
              />
            </Col>
            <Col>
              <FormattedMessage {...messages.resume} />
            </Col>
          </Row>
        </Button>
      </div>
      <hr />
      <H2>
        <FormattedMessage {...messages.social_media} />
      </H2>
      <hr />
      <Row className="text-center">
        <Col>
          <H1>
            <a target="_blank" href="https://gitlab.com/rdfedor">
              <FontAwesomeIcon icon={faGitlab} size="lg" />
            </a>
          </H1>
        </Col>
        <Col>
          <H1>
            <a target="_blank" href="https://www.linkedin.com/in/rdfedor/">
              <FontAwesomeIcon icon={faLinkedin} size="lg" />
            </a>
          </H1>
        </Col>
      </Row>
      <hr />
      <H2>
        <FormattedMessage {...messages.language} />
      </H2>
      <hr />
      <div className="text-center p-4">
        <LocaleToggle />
      </div>
    </DivPanel>
  );
}

Sidebar.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Sidebar;
