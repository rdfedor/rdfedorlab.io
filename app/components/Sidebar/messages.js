/*
 * Sidebar Messages
 *
 * This contains all the text for the Sidebar component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Sidebar';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Professional Details',
  },
  social_media: {
    id: `${scope}.social_media`,
    defaultMessage: 'Social Media',
  },
  attachments: {
    id: `${scope}.attachments`,
    defaultMessage: 'Attachments',
  },
  resume: {
    id: `${scope}.resume`,
    defaultMessage: 'Resume',
  },
  language: {
    id: `${scope}.language`,
    defaultMessage: 'Language',
  },
});
