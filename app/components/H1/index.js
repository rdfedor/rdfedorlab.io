/**
 *
 * H1
 *
 */

// import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const styledH1 = styled.h1`
  font-weight: bold;
  font-size: 2em;
  margin: 0.5em 0;
`;

export default styledH1;
