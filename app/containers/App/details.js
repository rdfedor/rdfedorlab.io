import messages from './messages';

export default [
  {
    label: messages.name,
    value: 'Roger Fedor',
  },

  {
    label: messages.location,
    value: 'Austin, TX',
  },
  {
    label: messages.experience,
    value: messages.experience_value,
  },
  {
    label: messages.degree,
    value: 'BSCS',
  },
  {
    label: messages.languages,
    value: messages.languages_value,
  },
];
