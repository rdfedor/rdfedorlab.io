/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.App';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Resume',
  },
  name: {
    id: `${scope}.name`,
    defaultMessage: 'Name',
  },
  location: {
    id: `${scope}.location`,
    defaultMessage: 'Location',
  },
  experience: {
    id: `${scope}.experience`,
    defaultMessage: 'Experience',
  },
  experience_value: {
    id: `${scope}.experience_value`,
    defaultMessage: '17 years',
  },
  degree: {
    id: `${scope}.degree`,
    defaultMessage: 'Degree',
  },
  languages: {
    id: `${scope}.language`,
    defaultMessage: 'Languages',
  },
  languages_value: {
    id: `${scope}.language_value`,
    defaultMessage: 'English / Castilian',
  },
});
