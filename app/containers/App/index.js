/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Row, Col, Container } from 'react-bootstrap';
import { FormattedMessage } from 'react-intl';

import AboutMePage from 'containers/AboutMePage/Loadable';
import RecommendationsPage from 'containers/RecommendationsPage/Loadable';
import ResumePage from 'containers/ResumePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

import TopMenu from '../../components/TopMenu';
import Sidebar from '../../components/Sidebar';
import H1 from '../../components/H1';

import GlobalStyle from '../../global-styles';

import messages from './messages';
import details from './details';
import { PortfolioPage } from '../PortfolioPage';

export default function App() {
  return (
    <div>
      <Helmet titleTemplate="%s - Roger Fedor">
        <title>Homepage</title>
        <meta
          name="description"
          content="A summary of my education, job history and recent projects."
        />
      </Helmet>
      <TopMenu />
      <Container>
        <H1 className="font-weight-bold text-uppercase">
          <FormattedMessage {...messages.header} />
        </H1>
        <Row>
          <Col md="4">
            <Sidebar details={details} />
          </Col>
          <Col>
            <Switch>
              <Route exact path="/" component={AboutMePage} />
              <Route path="/portfolio" component={PortfolioPage} />
              <Route path="/resume" component={ResumePage} />
              <Route path="/recommendations" component={RecommendationsPage} />
              <Route component={NotFoundPage} />
            </Switch>
          </Col>
        </Row>
      </Container>
      <GlobalStyle />
    </div>
  );
}
