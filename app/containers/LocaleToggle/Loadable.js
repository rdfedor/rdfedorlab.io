/**
 *
 * Asynchronously loads the component for LocaleToggle
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
