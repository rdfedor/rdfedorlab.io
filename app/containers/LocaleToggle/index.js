/**
 *
 * LocaleToggle
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createSelector } from 'reselect';
import { ButtonGroup, Button } from 'react-bootstrap';

import { appLocales } from '../../i18n';
import { changeLocale } from '../LanguageProvider/actions';
import { makeSelectLocale } from '../LanguageProvider/selectors';

export function LocaleToggle({ onLocaleToggle, locale }) {
  return (
    <ButtonGroup className="m-auto">
      {appLocales.map(appLocal => (
        <Button
          key={`language-${appLocal}`}
          variant="outline-secondary"
          value={appLocal}
          onClick={onLocaleToggle}
          active={locale === appLocal}
          className="text-uppercase"
        >
          {appLocal}
        </Button>
      ))}
    </ButtonGroup>
  );
}

LocaleToggle.propTypes = {
  onLocaleToggle: PropTypes.func,
  locale: PropTypes.string,
};

export function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onLocaleToggle: evt => dispatch(changeLocale(evt.target.value)),
  };
}

const mapStateToProps = createSelector(
  makeSelectLocale(),
  locale => ({
    locale,
  }),
);

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(LocaleToggle);
