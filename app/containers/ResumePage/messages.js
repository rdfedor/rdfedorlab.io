/*
 * ResumePage Messages
 *
 * This contains all the text for the ResumePage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ResumePage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Professional Experience',
  },
  education: {
    id: `${scope}.education`,
    defaultMessage: 'Education',
  },
  education_degree: {
    id: `${scope}.education_degree`,
    defaultMessage: 'Bachelors of Science in Computer Science',
  },
  education_focus: {
    id: `${scope}.education_focus`,
    defaultMessage: ' Emphasis in Information Assurance',
  },
  education_college: {
    id: `${scope}.education_college`,
    defaultMessage: 'University of Arkansas at Little Rock',
  },
  education_graduation: {
    id: `${scope}.education_graduation`,
    defaultMessage: 'December 2008 ',
  },
  education_degree_college_seperator: {
    id: `${scope}.education_degree_college_seperator`,
    defaultMessage: ' from ',
  },
});
