/**
 *
 * ResumePage
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';

import DivPanel from '../../components/DivPanel';
import H2 from '../../components/H2';
import ListPositions from '../../components/ListPositions';

import messages from './messages';
import positions from './positions';

export function ResumePage() {
  return (
    <>
      <Helmet>
        <title>Resume</title>
        <meta
          name="description"
          content="An overview of the positions I've held and activities across my various positions."
        />
      </Helmet>
      <DivPanel>
        <H2>
          <FormattedMessage {...messages.header} />
        </H2>
        <hr />
        <div className="pt-2">
          <ListPositions positions={positions} />
        </div>
      </DivPanel>
      <DivPanel>
        <H2>
          <FormattedMessage {...messages.education} />
        </H2>
        <div className="pl-4 pr-4 pb-4">
          <div>
            <span className="font-weight-bold font-italic">
              <FormattedMessage {...messages.education_degree} />
            </span>
            <FormattedMessage
              {...messages.education_degree_college_seperator}
            />
            <span className="font-weight-bold font-italic">
              <FormattedMessage {...messages.education_college} />
            </span>
          </div>
          <div>
            <FormattedMessage {...messages.education_graduation} />
            &#8212;
            <FormattedMessage {...messages.education_focus} />
          </div>
        </div>
      </DivPanel>
    </>
  );
}

ResumePage.propTypes = {};

export default ResumePage;
