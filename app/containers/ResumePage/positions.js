import optumLogo from '../../images/optum_logo.gif';
import frontgateLogo from '../../images/front-gate-tickets-logo.png';
import soteraLogo from '../../images/sotera-logo.jpg';
import ualrIeaLogo from '../../images/ualr-iea-logo.png';
import inmotionLogo from '../../images/inmotion_logo.png';

export default [
  {
    company: 'InMotion Software',
    image_src: inmotionLogo,
    duration: 'August 2022 - Present',
    location: 'Austin, TX (Remote)',
    position: 'Engineering Lead',
    highlights: [
      'Drafted architectural documents and oversaw changes across various services for white labeling the backend creating separate configurations to support customized white labeled Android and iOS apps.',
      'Drafted architectural documents and oversaw changes allowing for the easy addition of data for new mobile app features such as calibrating agricultural equipment to optimize crop yield or dynamically rendered guides based on user input incorporating videos, text and images in a step by step process.',
      'Mentored Android and iOS engineers on the creation of finite state machines for parsing strings. It included performing variable substitution for various boolean operations and handling multiple conditions improving performance by 108%.',
      'Planned and implemented the transition of existing microservices to a common framework using TypeScript w/ NextJS to simplify the tech stack, expand our hiring pool for engineers and reduce server resource consumption. It included coding style guides, automated unit test via pipeline integrations, self documenting API using Swagger and hot-reloading local dev environment.',
      'Design and implement a new deployment pattern in support the creation of a browser based version of the Android and iOS apps in ReactJS defining the structure of the project and incorporating automated enforcement of style guides and unit tests allowing to build and push static assets via Bitbucket Pipelines to Azure Storage to be served server-less via Azure CDN.',
    ],
  },
  {
    company: 'InMotion Software',
    image_src: inmotionLogo,
    duration: 'March 2020 - August 2022',
    location: 'Austin, TX (Remote)',
    position: 'Senior Software Engineer',
    highlights: [
      'Managed and mentored remote engineers in the development and maintenance of microservices across various languages including Kotlin, PHP, NodeJS and Typescript which served live telematics data for heavy machine equipment to mobile applications on Android and iOS.',
      'Architect and deploy Docker Swarm cluster with upgrade path to Kubernetes for microservices with fully automated BitBucket pipeline deployments that builds and push docker images to Azure Container Registry. Portainer is used to manage the cluster and deployments of stacks. It uses compose files from Git to stand up services with the versioned images created via the pipelines to allow infrastructure as code deployments paired with environmental variables secured in Azure Vault. This setup also allowed for functional testing through feature branch deployments with unique subdomains created based on branch names encrypted via SSL.',
      'Design and implement data synchronization process to update database model data.  Import profiles are created server side in PHP allowing for UI elements to be dynamically generated based on their design. A queue and background processing allows users to preview changes prior to committing via so frontend connections can be quickly opened and closed to serve other requests. Change logs are stored allowing for historical imports to be viewed and re-ran to revert changes.',
      'Ran daily scrum meetings and end of sprint retrospectives to identify problems and improve on existing processes.',
      'Drafted mocked ups and implemented a re-design of admin management pages for managing equipment and relationships eliminating the use of tables for design and improving overall page performance, readability and appeal.',
      'Refactored microservices from procedural to object oriented programing in NodeJS breaking up files with thousands of lines of code making it easier to read and more organized.',
    ],
  },
  {
    company: 'Optum, division of United Health Group',
    image_src: optumLogo,
    duration: 'June 2014 - March 2020',
    location: 'Austin, TX',
    position: 'Software Engineer',
    highlights: [
      'Worked as part of a team to develop an ETL solution using Jenkins, C# and Ms SQL Server to templatize identifying gaps in coding across 500,000 high risk patients cross-referencing patient clinical history data with billing data',
      'Implemented UI for AWS based EMR integration point to notify and collect physician feedback on patient risk assessment using NodeJS, HTML5, Bootstrap, ReactJS, Webpack and Jest',
      'Integrated gaps in coding with Microstrategy creating a multi-source reporting solution using MS SSIS to transfer data from the product to reporting databases',
      'Worked as part of a large team developing an enterprise level work flow application using CakePHP and PostgreSQL that manages care plans for more than 5 million patients',
      'Acted as Scrum Master managing a team of four engineers and one quality assurance tester',
      'Developed micro-service for creating Continuity of Care Documents using NodeJS, Express',
      'Developed multiple cross-platform applications using NodeJS, NW.js, HTML5, Bootstrap and AngularJS allowing changes to diagnosis based constant data and mappings within csv based files and reserve ids for XML based configurations storing database credentials using OpenPGP',
    ],
  },
  {
    company: 'Frontgate Ticketing Solutions',
    image_src: frontgateLogo,
    duration: 'January 2013 - June 2014',
    location: 'Austin, TX',
    position: 'Senior Software Engineer',
    highlights: [
      'Worked as part of a team in the design and development of a scalable, distributed multi-site e-commerce platform in PHP using Redis, Percona SQL and PHPResque capable of processing over $750,000 in sales per minute',
      "Charged with the implementation of the UI within the new distributed e-commerce system using HTML5, Bootstrap's Responsive Design and jQuery",
      "Lead the development of a cross-platform, Android/IOS, application using PhoneGap and ExtJS that clients used to validate their customer's physical and mobile tickets for events",
      "Charged with the development of a mobile delivery system for tickets which allowed customers to download their tickets through their smart phone using Apple's Passbook, or through a JPG",
      'Lead the development of an on-site festival support tool for clients written for Android using PhoneGap, Angular JS, HTML5 and Bootstrap for supporting customers with RFID wristbands',
    ],
  },
  {
    company: 'Sotera Defense Solutions',
    image_src: soteraLogo,
    duration: 'June 2011 - October 2012',
    location: 'Austin, TX',
    position: 'UI Engineer',
    highlights: [
      'Created an entity management interface handling Cloudbase entry manipulation which included adding, editing, deleting and managing relationships between entities using ExtJS/jQuery',
      'Developed interface migrating previous code away from using Google Earth plugin to OpenLayers reducing system requirements while maintaining existing functionality using jQuery UI',
      'Assisted developers in unifying widget layouts, images and terminology giving a consistent, familiar user experience across applications',
      'Closed out UI related issues in various widgets from JIRA Issue Management Systems',
      'Used GIT and SVN code repository solutions for code management',
      'Migrated widgets between team projects using different library versions',
    ],
  },
  {
    company: 'UALR: Institute for Economic Advancement',
    image_src: ualrIeaLogo,
    duration: 'February 2009 - June 2011',
    location: 'Little Rock, AR',
    position: 'Technical Analyst',
    highlights: [
      'Researched and implemented plans for the migration of client services to a cloud service reducing server maintenance costs $10,000 per year',
      'Mentored other developers and graduate assistants improving PHP programming practices and system security for future project developments',
      'Performed security audits on system code ensuring proper programming practices are followed',
      'Developed a secure, meta-data system for the aggregation and dispersion of statistical data in Arkansas using PHP, MySQL, jQuery and cross-site AJAX requests',
      'Credited in the publication of the 2010 Arkansas Personal Income Handbook',
    ],
  },
];
