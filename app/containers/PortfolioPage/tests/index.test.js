/**
 *
 * Tests for PortfolioPage
 *
 * @see https://github.com/react-boilerplate/react-boilerplate/tree/master/docs/testing
 *
 */

import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
// import 'jest-dom/extend-expect'; // add some helpful assertions

import { PortfolioPage } from '../index';
import { DEFAULT_LOCALE } from '../../../i18n';

describe('<PortfolioPage />', () => {
  it('Expect to not log errors in console', () => {
    const spy = jest.spyOn(global.console, 'error');
    const dispatch = jest.fn();
    render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <PortfolioPage dispatch={dispatch} />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('Expect gtag to log on clicking button', () => {
    const spy = jest.spyOn(global.console, 'error');
    const dispatch = jest.fn();

    const portfolioPageDom = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <PortfolioPage dispatch={dispatch} />
      </IntlProvider>,
    );
    expect(spy).not.toHaveBeenCalled();
    fireEvent.click(portfolioPageDom.getAllByText(/lcov report/i)[0]);
    expect(window.dataLayer).toBeDefined();
    expect(window.dataLayer.length).toBe(1);
  });

  /**
   * Unskip this test to use it
   *
   * @see {@link https://jestjs.io/docs/en/api#testskipname-fn}
   */
  it('Should render and match the snapshot', () => {
    const {
      container: { firstChild },
    } = render(
      <IntlProvider locale={DEFAULT_LOCALE}>
        <PortfolioPage />
      </IntlProvider>,
    );
    expect(firstChild).toMatchSnapshot();
  });
});
