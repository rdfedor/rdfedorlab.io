import {
  faCode,
  faBolt,
  faFilePdf,
  faCodeBranch,
} from '@fortawesome/free-solid-svg-icons';
import profileSitePic from '../../images/profileSite.png';
import gamers411Pic from '../../images/gamers411.png';
import gamers411Attachment from '../../documents/Fedor_Roger_D_-_Project_GamersInfoNet.pdf';
import messages from './messages';

export default [
  {
    title: 'This Site',
    image: profileSitePic,
    description:
      'A site built using NodeJS, HTML5, Bootstrap 4, ReactJS, Redux, Babel and WebPack to build static assets.  GitLab provides Continuous Integration / Continuous Delivery services which tests every component using React Testing Library and automaticaly deploys to GitLab pages when merged to master.',
    actions: [
      {
        label: messages.view_source,
        icon: faCode,
        href: 'https://gitlab.com/rdfedor/rdfedor.gitlab.io',
      },
      {
        label: messages.code_coverage,
        icon: faCodeBranch,
        href: 'https://rdfedor.gitlab.io/coverage',
      },
    ],
  },
  {
    title: 'DataHorde',
    description:
      "A data scraping and processing library with command line interface written in NodeJS leveraging the Sequelize interface to support MSSQL, MySQL, PSQL, MariaDB and SQLite.  Features GitLab Continuous Integration / Continuous Delivery which tests code using Mocha.  Once that passes, it builds the jsdoc documentation and code coverage reports and publishes the artifacts to NPM and GitLab pages where it's hosted when merged to master. Code coverage repots are generated using Instanbul.",
    actions: [
      {
        label: messages.documentation,
        icon: faBolt,
        href: 'https://rdfedor.gitlab.io/datahorde/',
      },
      {
        label: messages.view_source,
        icon: faCode,
        href: 'https://gitlab.com/rdfedor/datahorde',
      },
      {
        label: messages.code_coverage,
        icon: faCodeBranch,
        href: 'https://rdfedor.gitlab.io/datahorde/coverage',
      },
    ],
  },
  {
    title: 'Gamers411',
    image: gamers411Pic,
    description:
      'A scaleable, multi-site CMS built with using Varnish, Cloudflare CDN, PHP, Laravel, Memcache, beanstalkd for infrastruction and jQuery, Bootstrap and LESS for customized css to create a network of sites for gamers to communicate with one another.',
    actions: [
      {
        label: messages.website,
        icon: faBolt,
        href: 'http://nodeka.gamers411.net/',
      },
      {
        label: messages.documentation,
        icon: faFilePdf,
        href: gamers411Attachment,
      },
    ],
  },
];
