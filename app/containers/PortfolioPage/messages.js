/*
 * PortfolioPage Messages
 *
 * This contains all the text for the PortfolioPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.PortfolioPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Portfolio',
  },
  view_source: {
    id: `${scope}.view_source`,
    defaultMessage: 'View Source',
  },
  website: {
    id: `${scope}.website`,
    defaultMessage: 'Website',
  },
  documentation: {
    id: `${scope}.documentation`,
    defaultMessage: 'Documentation',
  },
  code_coverage: {
    id: `${scope}.code_coverage`,
    defaultMessage: 'LCOV Report',
  },
});
