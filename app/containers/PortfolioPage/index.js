/**
 *
 * PortfolioPage
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, Media, Image } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import md5 from 'md5';

import DivPanel from '../../components/DivPanel';
import H2 from '../../components/H2';
import H3 from '../../components/H3';
import { gtag } from '../../utils/gtag';

import portfolios from './portfolio';
import messages from './messages';

export function PortfolioPage() {
  return (
    <div>
      <Helmet>
        <title>Portfolio</title>
        <meta
          name="description"
          content="Examples of projects I've worked on using the knowledge I've learned over my career to build my skillset."
        />
      </Helmet>
      <DivPanel>
        <H2>
          <FormattedMessage {...messages.header} />
        </H2>
        <hr />
        {portfolios.map(portfolio => (
          <Media key={md5(JSON.stringify(portfolio))} className="p-4">
            <Media.Body>
              <H3 className="font-weight-bold">{portfolio.title}</H3>
              <p>{portfolio.description}</p>
              {portfolio.actions.map(action => (
                <Button
                  key={md5(JSON.stringify(action))}
                  className="mr-3"
                  href={action.href}
                  variant="outline-secondary"
                  target="_blank"
                  onClick={() => {
                    // Push notification to Google Analytics
                    gtag('event', 'portfolio_click', {
                      event_category: 'portfolio',
                      event_action: 'click',
                      event_label: `${portfolio.title} - ${
                        action.label.defaultMessage
                      }`,
                    });
                  }}
                >
                  <FontAwesomeIcon icon={action.icon} className="mr-2" />
                  <FormattedMessage {...action.label} />
                </Button>
              ))}
            </Media.Body>
            <Image
              width={175}
              className={`p-2 pt-3${!portfolio.image ? ' d-none' : ''}`}
              src={portfolio.image}
            />
          </Media>
        ))}
      </DivPanel>
    </div>
  );
}

PortfolioPage.propTypes = {};

export default PortfolioPage;
