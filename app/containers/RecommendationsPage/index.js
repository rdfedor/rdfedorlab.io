/**
 *
 * RecommendationsPage
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';

import DivPanel from '../../components/DivPanel';
import H2 from '../../components/H2';

import messages from './messages';
import recommendations from './recommendations';
import ListRecommendations from '../../components/ListRecommendations';

export function RecommendationsPage() {
  return (
    <div>
      <Helmet>
        <title>Recommendations</title>
        <meta
          name="description"
          content="What others I've worked with have said about me and my capabilities."
        />
      </Helmet>
      <DivPanel>
        <H2>
          <FormattedMessage {...messages.header} />
        </H2>
        <hr />
        <div className="pt-4">
          <ListRecommendations {...{ recommendations }} />
        </div>
      </DivPanel>
    </div>
  );
}

RecommendationsPage.propTypes = {};

export default RecommendationsPage;
