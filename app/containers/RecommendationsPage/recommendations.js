import jzhangPic from '../../images/recommendations/jzhang.jpeg';
import jmataPic from '../../images/recommendations/jmata.jpeg';
import bmezaPic from '../../images/recommendations/bmeza.jpeg';
import bmoorePic from '../../images/recommendations/bmoore.jpeg';
import rsekerPic from '../../images/recommendations/rseker.jpeg';
import sparmeleePic from '../../images/recommendations/sparmelee.jpeg';
import kostfeldPic from '../../images/recommendations/kostfeld.jpeg';
import dgarciaPic from '../../images/recommendations/dgarcia.jpeg';
import jjenkinsPic from '../../images/recommendations/jjenkins.jpeg';
import akostenarovPic from '../../images/recommendations/akostenarov.jpeg';
import jpahariPic from '../../images/recommendations/jpahari.jpeg';
import sramaswamyPic from '../../images/recommendations/sramaswamy.jpeg';
import vabburiPic from '../../images/recommendations/vabburi.jpeg';

export default [
  {
    name: 'Vinay Kumar Abburi',
    title:
      'Technology Leader | Software Architect @ DISCO | Data, Analytics, ML | Software & Distributed Systems',
    profilePic: vabburiPic,
    byLine: 'November 19, 2019, Vinay Kumar managed Roger directly',
    text:
      'Roger was a great developer who was able to handle anything i sent in his direction - frontend, infrastructure or backend. He always has a "can-do" positive attitude and strived to come up with the best solution based on the kind of problem at hand.',
  },
  {
    name: 'John Zhang',
    title: 'Chief Engineer at Golden Land, Inc.',
    profilePic: jzhangPic,
    byLine: 'July 16, 2016, John managed Roger directly',
    text:
      "Roger worked with me on a large scale healthcare application where he served as a team lead for a group of talented developers working to meet customer needs. He is very versatile in both front end and back end technologies and excelled in his role by leading team plowing through technical challenges sprint after sprint. I've found Roger very innovative, resourceful and persistent. He is a true team player and I have no slightest hesitance to have him on my team again!",
  },
  {
    name: 'Juan Pablo Mata',
    title: 'Software Engineer at Optum (formerly Alere Wellbeing)',
    profilePic: jmataPic,
    byLine: 'July 13, 2016, Roger worked with Juan Pablo in the same group',
    text:
      'Roger is a very talented and passionate Software Engineer. He was my mentor when I first started working at The Advisory Board Company. I had the pleasure of working with him and was able to learn from his vast experience. He is always eager to tackle new and challenging tasks as well as collaborate with other more senior engineers to help solved complex issues.',
  },
  {
    name: 'Ben Meza',
    title: 'Scrum Master at CSIdentity',
    profilePic: bmezaPic,
    byLine: 'June 3, 2014, Ben managed Roger directly',
    text:
      "Roger was a software developer on my team for the last year and a half and I can say he always brought a positive outlook to our projects and contributed a steady flow of ideas. I also appreciated that there was no task too big or too small for Roger - he did what was asked of him and always had a good attitude about it. Roger's willingness to help out wherever needed also made him a versatile developer in our department as we could quickly shift him to different projects where his skills could be utilized.",
  },
  {
    name: 'Brendan Moore',
    title: 'Helping digital brands with data-driven growth',
    profilePic: bmoorePic,
    byLine: 'June 2, 2014, Roger worked with Brendan in the same group',
    text:
      'Had the pleasure of having Roger on my dev team for over a year. He happily stepped into new challenges to best provide value to our team, including managing Bootstrap and advanced Google Analytics implementation. Always willing to help answer questions with a positive attitude, and always willing to share ideas.',
  },
  {
    name: 'Remzi Seker',
    title:
      'Associate Provost for Research at Embry-Riddle Aeronautical University',
    profilePic: rsekerPic,
    byLine: 'April 17, 2013, Remzi was Roger’s teacher',
    text:
      'Roger has been one of the most stellar students I have had. He is personable, polite, helpful to others, and has great research skills. I was always surprised with the ingenuity Roger presented. After he graduated, he worked in another on the campus and I had sent some students to work for him. Roger has also took on the role of mentoring those students. He is truly an asset and he always looks for ways to improve whatever it is that he does.',
  },
  {
    name: 'Sean Parmelee',
    title: 'Staff UI Engineer, Vrbo, part of Expedia Group',
    profilePic: sparmeleePic,
    byLine: 'November 6, 2012, Roger worked with Sean in the same group',
    text:
      "I had the pleasure of working with Roger on the same project for approximately 5 months. Roger's excellent communication skills made for an easy transition to his project. He is very passionate about his work. He actively seeks out feedback from others involved in the project including testers and UX designers to ensure quality. Roger is very knowledgeable with JavaScript and various JavaScript libraries including jQuery and ExtJS. He also contributed some of his work back to the jQuery project. I strongly recommend Roger Fedor.",
  },
  {
    name: 'Kurt Ostfeld',
    title: 'Senior Software Developer at Samba TV',
    profilePic: kostfeldPic,
    byLine: 'November 5, 2012, Roger worked with Kurt in the same group',
    text:
      'Roger worked as a web developer on the same team as myself. He has high technical aptitude and is proficient with many JavaScript frameworks. He worked well under pressure and was a very pleasant and productive person to work with.',
  },
  {
    name: 'David J. Garcia',
    title: 'Staff Big Data Engineer at Bazaarvoice',
    profilePic: dgarciaPic,
    byLine: 'November 3, 2012, Roger worked with David J. in the same group',
    text:
      'Roger is a highly competent web developer/engineer. He has detailed knowledge of javascript and associated frameworks. He is able to learn quickly and pays close attention to detail.',
  },
  {
    name: 'Jeremy Jenkins',
    title: 'Director of Engineering at OnPrem Solution Partners',
    profilePic: jjenkinsPic,
    byLine: 'October 29, 2012, Jeremy managed Roger directly',
    text:
      "In a roll of Chief Engineer and Team Lead, I directly managed Roger for approximately 6 months. While able to be put in roles where Roger works in isolation, Roger thrives more vibrantly in team dynamics where he can work actively with others. He is quick to seek-out and adjust-to feedback from fellow developers, team leads and product owners. Roger is a hard worker who's willing to put in extra effort to see his commitments succeed. He communicates easily and is a pleasure to work with on a day-to-day basis. He has a passion for technology, in particular, web UI trends. Not only does he understand technology, but he is able to execute on using that technology. I recommend Roger with full confidence and I would not hesitate to work with him again in the future.",
  },
  {
    name: 'Aleksandar Kostenarov, MBA',
    title: 'President at Savra Group LLC',
    profilePic: akostenarovPic,
    byLine: 'May 2, 2011, Aleksandar worked with Roger in different groups',
    text:
      'Roger is one of those people that every organization should be glad to have. He is very dependable, knowledgeable and efficient in everything he does. I had the pleasure of working with him on several occasions and he was absolutely helpful and provided an outstanding assistance in all the technical matters of our projects. While he is a true professional at his work he also does everything in a polite, courteous and friendly manner.',
  },
  {
    name: 'Jenish Pahari',
    title: 'Marketing/Business Consultant',
    profilePic: jpahariPic,
    byLine: 'May 1, 2011, Jenish worked with Roger in different groups',
    text:
      'Roger is detail oriented, attentive and a great communicator. He is a bright, dedicated young professional with ability to grasp and integrate new concepts and ways of doing things with ease. I have collaborated with Roger on several projects, and he is an absolute pleasure to work with. He supplied me not only with timely and accurate data sets, but also details and insights which helped me produce my analysis. I would recommend him as he would be a great asset to any organization.',
  },
  {
    name: 'Srini Ramaswamy',
    title:
      'R&D Technology Head for Innovation, Software and Analytics, BU Power Generation (Industrial Automation)',
    profilePic: sramaswamyPic,
    byLine: 'April 28, 2011, Srini was Roger’s teacher',
    text:
      "Roger Fedor has been in the top 10% of all students (of over 1000's), that I have taught / mentored through my career.He has a keen interest on improving his skills and the necessary aptitude to work hard towards his goals. He has demonstrated good problem solving skills and he never hesitated to jump in on projects that were considered more risky (vis-a-vis grades) by other students. His ability to seek out effective solutions for such work was very note worthy. While mentoring him during his senior project, he demonstrated an excellence in every aspect of Software Engineering / Development through the requirments specifications, development using the right set of tools, platforms and environment to documentation. He will make an exceptional asset to any group he chooses to associate with.",
  },
];
