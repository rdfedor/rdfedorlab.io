/**
 *
 * Asynchronously loads the component for RecommendationsPage
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
