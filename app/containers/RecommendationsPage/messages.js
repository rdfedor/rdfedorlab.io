/*
 * RecommendationsPage Messages
 *
 * This contains all the text for the RecommendationsPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.RecommendationsPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Recommendations',
  },
});
