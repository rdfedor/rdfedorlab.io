/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.NotFoundPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Whoops! Looks like you followed a link to a dead end.',
  },
  page_not_found_message: {
    id: `${scope}.page_not_found_message`,
    defaultMessage:
      'The page you are looking for could not be found.  Please check the link is correct and try again.',
  },
  page_not_found_status: {
    id: `${scope}.page_not_found_status`,
    defaultMessage: 'Status: 404 Page not found',
  },
});
