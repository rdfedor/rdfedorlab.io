/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Image, Row, Col } from 'react-bootstrap';

import DivPanel from '../../components/DivPanel';
import messages from './messages';
import whoopsImage from '../../images/whoops.png';

export default function NotFound() {
  return (
    <DivPanel>
      <Row className="text-center p-4">
        <Col md="4">
          <Image src={whoopsImage} width={200} fluid />
        </Col>
        <Col>
          <h1>
            <FormattedMessage {...messages.header} />
          </h1>
          <p>
            <FormattedMessage {...messages.page_not_found_message} />
          </p>
          <p className="text-muted text-italic">
            <small>
              <FormattedMessage {...messages.page_not_found_status} />
            </small>
          </p>
        </Col>
      </Row>
    </DivPanel>
  );
}
