import messages from './messages';

export default [
  {
    name: messages.html5,
    value: 80,
  },
  {
    name: messages.bootstrap,
    value: 80,
  },
  {
    name: messages.react,
    value: 55,
  },
  {
    name: messages.azure,
    value: 35,
  },
  {
    name: messages.javascript,
    test: /(javascript|jquery|angularjs|extjs|node|react)/i,
    value: 70,
  },
  {
    name: messages.nodejs,
    value: 65,
  },
  {
    name: messages.php,
    value: 75,
  },
  {
    name: messages.sql,
    value: 70,
  },
];
