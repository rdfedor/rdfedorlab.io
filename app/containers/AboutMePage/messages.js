/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.HomePage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'About Me',
  },
  i_am: {
    id: `${scope}.i_am`,
    defaultMessage: 'An Information Technology Specialist',
  },
  skills: {
    id: `${scope}.skills`,
    defaultMessage: 'Skills',
  },
  html5: {
    id: `${scope}.html5`,
    defaultMessage: 'HTML5',
  },
  bootstrap: {
    id: `${scope}.bootstrap`,
    defaultMessage: 'Bootstrap',
  },
  react: {
    id: `${scope}.react`,
    defaultMessage: 'React',
  },
  javascript: {
    id: `${scope}.javascript`,
    defaultMessage: 'Javascript',
  },
  nodejs: {
    id: `${scope}.nodejs`,
    defaultMessage: 'NodeJS',
  },
  php: {
    id: `${scope}.php`,
    defaultMessage: 'PHP',
  },
  sql: {
    id: `${scope}.sql`,
    defaultMessage: 'SQL',
  },
  introduction: {
    id: `${scope}.introduction`,
    defaultMessage:
      "Known for my eagerness for self improvement through the learning of new research materials, and the willingness to share what I've learned with others, I help provide a group with a wide variety of abilities to tackle whatever task may be needed to reach project goals. Excelling in problem solving skills, I look at any situation from various angles to find the most efficient solution available.",
  },
  azure: {
    id: `${scope}.azure`,
    defaultMessage: 'Azure',
  },
});
