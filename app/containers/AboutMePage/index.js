/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import { Helmet } from 'react-helmet';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import DivPanel from '../../components/DivPanel';
import H2 from '../../components/H2';
import ListDetails from '../../components/ListDetails';
import ListSkill from '../../components/ListSkills';
import details from '../App/details';
import positions from '../ResumePage/positions';

import messages from './messages';
import skills from './skills';

export default function AboutMePage() {
  return (
    <>
      <Helmet>
        <title>About Me</title>
        <meta
          name="description"
          content="A brief introduction to who I am and highlight some of my more noteable capabilities."
        />
      </Helmet>
      <DivPanel>
        <H2>
          <FormattedMessage {...messages.header} />
        </H2>
        <hr />
        <ListDetails location="home" details={details} groupBy={2} />
      </DivPanel>
      <DivPanel>
        <H2>
          <FormattedMessage {...messages.i_am} />
        </H2>
        <hr />
        <div className="pt-2 pl-4 pr-4 pb-2">
          <p>
            <FormattedMessage {...messages.introduction} />
          </p>
        </div>
      </DivPanel>
      <DivPanel>
        <H2>
          <FormattedMessage {...messages.skills} />
        </H2>
        <hr />
        <ListSkill
          {...{ skills: skills.sort((a, b) => b.value - a.value), positions }}
        />
      </DivPanel>
    </>
  );
}
