import history from '../history';

describe('history', () => {
  it('history should push to window.datalayer on push', () => {
    history.push('/test');
    expect(window.dataLayer).toBeDefined();
    expect(window.dataLayer.length).toBe(1);
  });
});
