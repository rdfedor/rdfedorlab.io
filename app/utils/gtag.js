// Initialize mock Google Analytics interface
window.dataLayer = window.dataLayer || [];

if (typeof window.gtag === 'undefined') {
  window.gtag = (...args) => {
    window.dataLayer.push(args);
  };
}

if (typeof window.gtagPageview === 'undefined') {
  window.gtagPageview = path => {
    window.gtag('config', 'test id', { page_path: path });
  };
}

/**
 * Triggers gtag request
 *
 * @export
 * @param {*} args
 */
export function gtag(...args) {
  window.gtag(...args);
}

/**
 * Triggers gtagPageView request
 *
 * @export
 * @param {*} args
 */
export function gtagPageView(...args) {
  window.gtagPageview(...args);
}
