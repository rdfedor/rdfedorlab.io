import { createHashHistory } from 'history';
import { gtagPageView } from './gtag';
const history = createHashHistory();

history.listen(location => {
  gtagPageView(location.pathname + location.search);
});

export default history;
