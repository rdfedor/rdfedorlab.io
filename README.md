
### My Resume Site

This project aims to show some of my capabilities in the area of UI development using React, Redux,
Bootstrap 4 and Webpack to stand up a development environment with Hot Module Reloading.

#### Requirements

- [Node.js](https://nodejs.org/en/)


#### Installation 

$	git clone https://gitlab.com/rdfedor/rdfedor.gitlab.io.git 
$	cd rdfedor.gitlab.io
$	npm install


#### Start Development Environment 

$ npm start

Once started, open your browser to http://localhost:3000/

#### Running Test Cases

$ npm test

#### Building Static Assets

$ npm run build

#### License 

My Resume Site is licensed under the MIT [License](LICENSE).


